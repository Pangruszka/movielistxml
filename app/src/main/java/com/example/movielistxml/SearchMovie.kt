package com.example.movielistxml

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import android.util.Log

class SearchMovie : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_search_movie)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        val inputMovie: EditText = findViewById(R.id.searchMovieText)
        val showMovieListButton: Button = findViewById(R.id.searchMovieButton)
        showMovieListButton.setOnClickListener{
            val movieName = inputMovie.text.toString()
            val jsonOpenerTest = JsonOpener("""C:\Users\jkkru\AndroidStudioProjects\MovieListXml\app\src\main\assets\movieList.json5""")
            Log.d("Search movie ",jsonOpenerTest.getOpenedJsonArray().toString())
        }




    }
}