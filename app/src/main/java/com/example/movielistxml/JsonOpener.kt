package com.example.movielistxml

import org.json.JSONArray
import java.io.File
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonParser


class JsonOpener (path: String){
    private val file = File(path)
    private val jsonString = file.readText()
    val jsonArray: JsonArray? = JsonParser.parseString(jsonString).asJsonArray
    fun getOpenedJsonArray(): JsonArray? {
        return jsonArray
    }
}