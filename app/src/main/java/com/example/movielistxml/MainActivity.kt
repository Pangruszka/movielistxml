package com.example.movielistxml

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val searchMovieButton: Button = findViewById(R.id.searchMovie)

        searchMovieButton.setOnClickListener{
            val intent = Intent(this, SearchMovie::class.java)
            startActivity(intent)
        }




    }
}